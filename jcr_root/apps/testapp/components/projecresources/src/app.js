angular.module('MarsApp',[]);


function MarsFactory(){
    var clients = [
        {
            id: 1,
            firstName: 'John',
            lastName: 'Doe'
        },
        {
            id: 2,
            firstName: 'Johnny',
            lastName: 'Does'
        },
        ];
    return clients;    
}

angular.module('MarsApp')
.factory('MarsFactory',MarsFactory);


angular.module('MarsApp')
    .controller('MarsController',MarsController);


MarsController.$inject = ['MarsFactory'];


function MarsController(MarsFactory){
    this.title = 'From Mars';
    this.clients = MarsFactory;
}